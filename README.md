ircrt
=====

An IRC client library for the WinRT API

License
=======

Released under the terms of the MIT License. Please see the LICENSE file in this repository.

Author
======

Robert Roland <robert@robertroland.org>

Copyright (c) 2012 Robert Roland