﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Commands
{
    public class WhoIs : IIRCCommand
    {
        private string _nick;
        public string Nick
        {
            get { return this._nick; }
        }

        public WhoIs(string nick)
        {
            this._nick = nick;
        }

        public IRCMessage BuildMessage()
        {
            return new IRCMessage(null, "WHOIS", this.Nick);
        }
    }
}
