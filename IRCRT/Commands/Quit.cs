﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Commands
{
    public class Quit : IIRCCommand
    {
        private string _message;
        public string Message
        {
            get { return this._message; }
        }

        public Quit(string message)
        {
            this._message = message;
        }

        public IRCMessage BuildMessage()
        {
            return new IRCMessage(null, "QUIT", Message.Split(' '));
        }
    }
}
