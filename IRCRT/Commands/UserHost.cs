﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Commands
{
    public class UserHost : IIRCCommand
    {
        private List<string> _nicks = new List<string>();
        public List<string> Nicks
        {
            get { return this._nicks; }
        }

        public UserHost() { }

        public IRCMessage BuildMessage()
        {
            return new IRCMessage(null, "USERHOST", Nicks.Select(n => n).ToArray());
        }
    }
}
