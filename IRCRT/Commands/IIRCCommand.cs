﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Commands
{
    public interface IIRCCommand
    {
        IRCMessage BuildMessage();
    }
}
