﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Models
{
    public class IRCUserChannel : BindableBase
    {
        private string _nick;
        public string Nick
        {
            get { return this._nick; }
            set { this.SetProperty(ref this._nick, value); }
        }

        private bool _operator;
        public bool Operator
        {
            get { return this._operator; }
            set { this.SetProperty(ref this._operator, value); }
        }

        private bool _canSpeak;
        public bool CanSpeak
        {
            get { return this._canSpeak; }
            set { this.SetProperty(ref this._canSpeak, value); }
        }

        private string _channel;
        public string Channel
        {
            get { return this._channel; }
            set { this.SetProperty(ref this._channel, value); }
        }

        public IRCUserChannel() { }
    }
}
