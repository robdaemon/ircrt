﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.Networking;
using Windows.Networking.Sockets;
using Windows.Storage.Streams;
using Windows.System.Threading;

using IRCRT.Events;
using IRCRT.Commands;
using IRCRT.Models;

namespace IRCRT
{
    public partial class IRCClient : BindableBase, IDisposable 
    {
        private const char prefixChar = ':';
        private const char delimiter = ' ';
        private const char lineEnding = '\n';

        private const string prefixString = ":";
        private const string delimiterString = " ";
        private const string lineEndingString = "\n";

        private const char carriageReturn = '\r';

        private StreamSocket ircSocket;

        private string host;
        private int port;
        private bool ssl;

        private string _currentNick;
        public string CurrentNick
        {
            get { return this._currentNick; }
            private set { this.SetProperty(ref this._currentNick, value); }
        }

        private Dictionary<string, Action<IRCMessage>> responses = new Dictionary<string, Action<IRCMessage>>();

        public IRCClient(string host, int port, bool ssl)
        {
            ircSocket = new StreamSocket();

            this.host = host;
            this.port = port;
            this.ssl = ssl;

            InitializeProcessors();
        }

        private void InitializeProcessors()
        {
            responses.Add("NOTICE", s => this.OnNotice(s));
            responses.Add("PING", s => this.OnPing(s));
            responses.Add("ERROR", s => this.OnError(s));
            responses.Add("PRIVMSG", s => this.OnMessage(s));

            responses.Add("001", s => this.OnWelcome(s));
            responses.Add("002", s => this.OnYourHost(s));
            responses.Add("003", s => this.OnCreated(s));
            responses.Add("004", s => this.OnMyInfo(s));
            responses.Add("005", s => this.OnBounce(s));

            responses.Add("302", s => this.OnUserHost(s));
            responses.Add("303", s => this.OnIsOn(s));
            responses.Add("301", s => this.OnAway(s));
            responses.Add("305", s => this.OnUnAway(s));
            responses.Add("306", s => this.OnNowAway(s));
            responses.Add("311", s => this.OnWhoIsUser(s));
            responses.Add("312", s => this.OnWhoIsServer(s));
            responses.Add("313", s => this.OnWhoIsOperator(s));
            responses.Add("317", s => this.OnWhoIsIdle(s));
            responses.Add("318", s => this.OnEndOfWhoIs(s));
            responses.Add("319", s => this.OnWhoIsChannels(s));
            responses.Add("314", s => this.OnWhoWasUser(s));
            responses.Add("369", s => this.OnEndOfWhoWas(s));

            responses.Add("251", s => this.OnLocalUsersCounts(s));
            responses.Add("252", s => this.OnLocalUserOperators(s));
            responses.Add("253", s => this.OnLocalUnknownUsers(s));
            responses.Add("254", s => this.OnLocalUserChannels(s));
            responses.Add("255", s => this.OnLocalUsers(s));
            responses.Add("256", s => this.OnAdminMessage(s));
            responses.Add("257", s => this.OnAdminLocation(s));
            responses.Add("258", s => this.OnAdminLocation(s));
            responses.Add("259", s => this.OnAdminEmail(s));

            responses.Add("263", s => this.OnTryAgain(s));

            responses.Add("401", s => this.OnNoSuchNick(s));
            responses.Add("402", s => this.OnNoSuchServer(s));
            responses.Add("403", s => this.OnNoSuchChannel(s));
            responses.Add("404", s => this.OnCannotSendToChannel(s));
            responses.Add("405", s => this.OnTooManyChannels(s));
            responses.Add("406", s => this.OnWasNoSuchNick(s));
            responses.Add("407", s => this.OnTooManyTargets(s));
            responses.Add("408", s => this.OnNoSuchService(s));
            responses.Add("409", s => this.OnNoOriginSpecified(s));
            responses.Add("411", s => this.OnNoRecipientGiven(s));
            responses.Add("412", s => this.OnNoTextToSend(s));
            responses.Add("413", s => this.OnNoToplevelDomainSpecified(s));
            responses.Add("414", s => this.OnWildcardInToplevelDomain(s));
            responses.Add("415", s => this.OnBadMask(s));
            responses.Add("421", s => this.OnUnknownCommand(s));
            responses.Add("422", s => this.OnNoMotd(s));
            responses.Add("423", s => this.OnNoAdminInfo(s));
            responses.Add("424", s => this.OnFileError(s));
            responses.Add("431", s => this.OnNoNicknameGiven(s));
            responses.Add("432", s => this.OnErroneousNickname(s));
            responses.Add("433", s => this.OnNicknameInUse(s));
            responses.Add("436", s => this.OnNicknameCollision(s));
            responses.Add("437", s => this.OnUnavailableResource(s));
            responses.Add("441", s => this.OnUserNotInChannel(s));
            responses.Add("442", s => this.OnNotOnChannel(s));
            responses.Add("443", s => this.OnUserAlreadyOnChannel(s));
            responses.Add("444", s => this.OnNotLoggedIn(s));
            responses.Add("445", s => this.OnSummonDisabled(s));
            responses.Add("446", s => this.OnUsersDisabled(s));
            responses.Add("451", s => this.OnNotRegistered(s));
            responses.Add("461", s => this.OnNeedMoreParameters(s));
            responses.Add("462", s => this.OnAlreadyRegistered(s));
            responses.Add("463", s => this.OnUnprivilegedHost(s));
            responses.Add("464", s => this.OnPasswordMismatch(s));
            responses.Add("465", s => this.OnBanned(s));
            responses.Add("466", s => this.OnYouWillBeBanned(s));
            responses.Add("467", s => this.OnChannelKeyAlreadySet(s));
            responses.Add("471", s => this.OnChannelIsFull(s));
            responses.Add("472", s => this.OnUnknownChannelMode(s));
            responses.Add("473", s => this.OnInviteOnlyChannel(s));
            responses.Add("474", s => this.OnBannedFromChannel(s));
            responses.Add("475", s => this.OnBadChannelKey(s));
            responses.Add("476", s => this.OnBadChannelMask(s));
            responses.Add("477", s => this.OnChannelModesUnsupported(s));
            responses.Add("478", s => this.OnBanListFull(s));
            responses.Add("481", s => this.OnNoPrivileges(s));
            responses.Add("482", s => this.OnChannelOperatorPrivilegeNeeded(s));
            responses.Add("483", s => this.OnCannotKillServer(s));
            responses.Add("484", s => this.OnRestrictedConnection(s));
            responses.Add("485", s => this.OnNotOriginalChannelOperator(s));
            responses.Add("491", s => this.OnNotOperatorHost(s));
            responses.Add("501", s => this.OnUnknownModeFlag(s));
            responses.Add("502", s => this.OnNotYourUser(s));
        }

        public async void Connect()
        {
            await ircSocket.ConnectAsync(
                new HostName(this.host),
                this.port.ToString(),
                this.ssl ? SocketProtectionLevel.Ssl : SocketProtectionLevel.PlainSocket);

            OnConnected();

            InboundProcessor();
        }

        private async void InboundProcessor()
        {
            StringBuilder buffer = new StringBuilder();
            try
            {
                using (DataReader reader = new DataReader(this.ircSocket.InputStream))
                {
                    reader.InputStreamOptions = InputStreamOptions.Partial;

                    while (true)
                    {
                        var inboundLength = await reader.LoadAsync(512);

                        if (inboundLength == 0)
                        {
                            // this socket is closed.

                            var handler = this.Disconnected;
                            handler(this, new IRCDisconnectEventArgs("server terminated connection"));

                            return;
                        }

                        string str = reader.ReadString(inboundLength);

                        foreach(char inboundChar in str) {
                            if (inboundChar.Equals(lineEnding))
                            {
                                string fullCommand = buffer.ToString();

                                string[] messageParts = fullCommand.Split(delimiter);

                                string prefix = null;
                                string command = null;
                                string[] parameters = null;
                                int parameterStartIndex = 0;
                                int parameterSize = 0;

                                if (fullCommand[0] == prefixChar)
                                {
                                    prefix = messageParts[0].Substring(1);
                                    
                                    parameterStartIndex = 2;
                                    parameterSize = messageParts.Length - 2;
                                }
                                else
                                {                                    
                                    parameterStartIndex = 1;
                                    parameterSize = messageParts.Length - 1;
                                }

                                command = messageParts[parameterStartIndex - 1];
                                parameters = new string[parameterSize];
                                Array.Copy(messageParts, parameterStartIndex, parameters, 0, parameters.Length);

                                DispatchMessage(new IRCMessage(prefix, command, parameters));
                                buffer.Clear();
                            }
                            else
                            {
                                // ditch the carriage return if we see it.
                                if(inboundChar != carriageReturn)
                                    buffer.Append(inboundChar);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                if (SocketError.GetStatus(ex.HResult) == SocketErrorStatus.Unknown)
                {
                    throw;
                }

                System.Diagnostics.Debug.WriteLine("Read stream failed with error: " + ex.Message);
            }
        }

        public void SendCommand(IIRCCommand command)
        {
            SendMessage(command.BuildMessage());
        }

        private async void SendMessage(IRCMessage message)
        {
            System.Diagnostics.Debug.WriteLine("Sending message: " + message);

            DataWriter writer = new DataWriter(this.ircSocket.OutputStream);

            if (message.Prefix != null)
            {
                writer.WriteString(prefixString);
                writer.WriteString(message.Prefix);
                writer.WriteString(delimiterString);
            }

            writer.WriteString(message.Command);
            
            if (message.Parameters != null)
            {
                writer.WriteString(delimiterString);
                for (int i = 0; i < message.Parameters.Length; i++)
                {
                    writer.WriteString(message.Parameters[i]);
                    if (i < message.Parameters.Length)
                        writer.WriteString(delimiterString);
                }
            }

            writer.WriteString(lineEndingString);

            var ret = await writer.StoreAsync();
            writer.DetachStream();
        }

        private void DispatchMessage(IRCMessage message)
        {
            if (responses.ContainsKey(message.Command))
            {
                Action<IRCMessage> processor = responses[message.Command];

                System.Diagnostics.Debug.WriteLine("Dispatching message " + message);

                processor.Invoke(message);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("No processor for message " + message);
            }
        }

        public event EventHandler<EventArgs> Connected;
        public event EventHandler<IRCDisconnectEventArgs> Disconnected;
        public event EventHandler<IRCNoticeEventArgs> Notice;
        public event EventHandler<IRCMessageEventArgs> Message;
        public event EventHandler<IRCPingEventArgs> Ping;
        public event EventHandler<IRCDisconnectEventArgs> Error;
        public event EventHandler<IRCWelcomeEventArgs> Welcome;
        public event EventHandler<IRCYourHostEventArgs> YourHost;
        public event EventHandler<IRCCreatedEventArgs> Created;
        public event EventHandler<IRCMyInfoEventArgs> MyInfo;
        public event EventHandler<IRCBounceEventArgs> Bounce;
        public event EventHandler<IRCServerMessageEventArgs> LocalUsersCounts;
        public event EventHandler<IRCServerMessageEventArgs> LocalUserOperators;
        public event EventHandler<IRCServerMessageEventArgs> LocalUnknownUsers;
        public event EventHandler<IRCServerMessageEventArgs> LocalUserChannels;
        public event EventHandler<IRCServerMessageEventArgs> LocalUsers;
        public event EventHandler<IRCServerMessageEventArgs> AdminMessage;
        public event EventHandler<IRCServerMessageEventArgs> AdminLocation;
        public event EventHandler<IRCServerMessageEventArgs> AdminEmail;
        public event EventHandler<IRCServerMessageEventArgs> TryAgain;
        public event EventHandler<IRCTargetedMessageEventArgs> NoSuchNick;
        public event EventHandler<IRCTargetedMessageEventArgs> NoSuchServer;
        public event EventHandler<IRCTargetedMessageEventArgs> NoSuchChannel;
        public event EventHandler<IRCTargetedMessageEventArgs> CannotSendToChannel;
        public event EventHandler<IRCTargetedMessageEventArgs> TooManyChannels;
        public event EventHandler<IRCTargetedMessageEventArgs> WasNoSuchNick;
        public event EventHandler<IRCTargetedMessageEventArgs> TooManyTargets;
        public event EventHandler<IRCTargetedMessageEventArgs> NoSuchService;
        public event EventHandler<IRCServerMessageEventArgs> NoOriginSpecified;
        public event EventHandler<IRCServerMessageEventArgs> NoRecipientGiven;
        public event EventHandler<IRCServerMessageEventArgs> NoTextToSend;
        public event EventHandler<IRCTargetedMessageEventArgs> NoToplevelDomainSpecified;
        public event EventHandler<IRCTargetedMessageEventArgs> WildcardInToplevelDomain;
        public event EventHandler<IRCTargetedMessageEventArgs> BadMask;
        public event EventHandler<IRCTargetedMessageEventArgs> UnknownCommand;
        public event EventHandler<IRCServerMessageEventArgs> NoMotd;
        public event EventHandler<IRCServerMessageEventArgs> NoAdminInfo;
        public event EventHandler<IRCServerMessageEventArgs> FileError;
        public event EventHandler<IRCServerMessageEventArgs> NoNicknameGiven;
        public event EventHandler<IRCTargetedMessageEventArgs> ErroneousNickname;
        public event EventHandler<IRCTargetedMessageEventArgs> NicknameInUse;
        public event EventHandler<IRCTargetedMessageEventArgs> NicknameCollision;
        public event EventHandler<IRCTargetedMessageEventArgs> UnavailableResource;
        public event EventHandler<IRCNickChannelMessageEventArgs> UserNotInChannel;
        public event EventHandler<IRCTargetedMessageEventArgs> NotOnChannel;
        public event EventHandler<IRCNickChannelMessageEventArgs> UserAlreadyOnChannel;
        public event EventHandler<IRCTargetedMessageEventArgs> NotLoggedIn;
        public event EventHandler<IRCServerMessageEventArgs> SummonDisabled;
        public event EventHandler<IRCServerMessageEventArgs> UsersDisabled;
        public event EventHandler<IRCTargetedMessageEventArgs> NeedMoreParameters;
        public event EventHandler<IRCNotRegisteredEventArgs> NotRegistered;
        public event EventHandler<IRCServerMessageEventArgs> AlreadyRegistered;
        public event EventHandler<IRCServerMessageEventArgs> UnprivilegedHost;
        public event EventHandler<IRCServerMessageEventArgs> PasswordMismatch;
        public event EventHandler<IRCServerMessageEventArgs> Banned;
        public event EventHandler<IRCServerMessageEventArgs> YouWillBeBanned;
        public event EventHandler<IRCTargetedMessageEventArgs> ChannelKeyAlreadySet;
        public event EventHandler<IRCTargetedMessageEventArgs> ChannelIsFull;
        public event EventHandler<IRCTargetedMessageEventArgs> UnknownChannelMode;
        public event EventHandler<IRCTargetedMessageEventArgs> InviteOnlyChannel;
        public event EventHandler<IRCTargetedMessageEventArgs> BannedFromChannel;
        public event EventHandler<IRCTargetedMessageEventArgs> BadChannelKey;
        public event EventHandler<IRCTargetedMessageEventArgs> BadChannelMask;
        public event EventHandler<IRCTargetedMessageEventArgs> ChannelModesUnsupported;
        public event EventHandler<IRCTargetedMessageEventArgs> BanListFull;
        public event EventHandler<IRCServerMessageEventArgs> NoPrivileges;
        public event EventHandler<IRCTargetedMessageEventArgs> ChannelOperatorPrivilegeNeeded;
        public event EventHandler<IRCServerMessageEventArgs> CannotKillServer;
        public event EventHandler<IRCServerMessageEventArgs> RestrictedConnection;
        public event EventHandler<IRCServerMessageEventArgs> NotOriginalChannelOperator;
        public event EventHandler<IRCServerMessageEventArgs> NotOperatorHost;
        public event EventHandler<IRCServerMessageEventArgs> UnknownModeFlag;
        public event EventHandler<IRCServerMessageEventArgs> NotYourUser;
        public event EventHandler<IRCUserHostEventArgs> UserHost;
        public event EventHandler<IRCIsOnEventArgs> IsOn;
        public event EventHandler<IRCAwayEventArgs> Away;
        public event EventHandler<IRCServerMessageEventArgs> UnAway;
        public event EventHandler<IRCServerMessageEventArgs> NowAway;
        public event EventHandler<IRCWhoIsUserEventArgs> WhoIsUser;
        public event EventHandler<IRCWhoIsServerEventArgs> WhoIsServer;
        public event EventHandler<IRCTargetedMessageEventArgs> WhoIsOperator;
        public event EventHandler<IRCWhoIsIdleEventArgs> WhoIsIdle;
        public event EventHandler<IRCTargetedMessageEventArgs> EndOfWhoIs;
        public event EventHandler<IRCWhoWasUserEventArgs> WhoWasUser;
        public event EventHandler<IRCTargetedMessageEventArgs> EndOfWhoWas;
        public event EventHandler<IRCWhoIsChannelsEventArgs> WhoIsChannels;

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        private void OnConnected()
        {
            var handler = this.Connected;
            if (handler != null)
                handler(this, null);
        }

        private void OnNotice(IRCMessage message)
        {
            System.Diagnostics.Debug.WriteLine("OnNotice: " + message.Parameters);

            string target = message.Parameters[0];
            string messageText = String.Join(" ", message.Parameters, 1, message.Parameters.Length - 1);

            var handler = this.Notice;
            if (handler != null)
                handler(this, new IRCNoticeEventArgs(target, messageText));
        }

        private void OnPing(IRCMessage message)
        {
            System.Diagnostics.Debug.WriteLine("OnPing: " + message.Parameters);

            string requestor = message.Parameters[0].Substring(1);

            var handler = this.Ping;
            if(handler != null)
                handler(this, new IRCPingEventArgs(requestor));

            Pong(requestor);
        }

        private void OnError(IRCMessage message)
        {
            string messageText = String.Join(" ", message.Parameters);

            var handler = this.Error;
            if (handler != null)
                handler(this, new IRCDisconnectEventArgs(messageText));
        }

        private void OnMessage(IRCMessage message)
        {
            System.Diagnostics.Debug.WriteLine("OnMessage: " + message.Parameters);

            string recipient = message.Parameters[0];
            string sender = message.Prefix;
            string senderNickname = sender.Substring(0, sender.IndexOf("!"));
            string senderUser = sender.Substring(sender.IndexOf("!"));
            string messageText = String.Join(" ", message.Parameters, 1, message.Parameters.Length - 1).Substring(1);

            var handler = this.Message;
            if (handler != null)
                handler(this, new IRCMessageEventArgs(recipient, senderNickname, senderUser, messageText));
        }

        private void OnWelcome(IRCMessage message)
        {
            string recipient = message.Parameters[0];

            var handler = this.Welcome;
            if(handler != null)
                handler(this, new IRCWelcomeEventArgs(recipient, TargetedMessageExtractor(message)));
        }

        private void OnYourHost(IRCMessage message)
        {
            string recipient = message.Parameters[0];

            var handler = this.YourHost;
            if (handler != null)
                handler(this, new IRCYourHostEventArgs(recipient, TargetedMessageExtractor(message)));
        }

        private void OnCreated(IRCMessage message)
        {
            string recipient = message.Parameters[0];

            var handler = this.Created;
            if (handler != null)
                handler(this, new IRCCreatedEventArgs(recipient, TargetedMessageExtractor(message)));
        }

        private void OnMyInfo(IRCMessage message)
        {
            string recipient = message.Parameters[0];

            var handler = this.MyInfo;
            if (handler != null)
                handler(this, new IRCMyInfoEventArgs(recipient, TargetedMessageExtractor(message)));
        }

        private void OnNotRegistered(IRCMessage message)
        {
            var handler = this.NotRegistered;
            if (handler != null)
                handler(this, new IRCNotRegisteredEventArgs(GenericMessageExtractor(message)));
        }

        private void OnBounce(IRCMessage message)
        {
            var handler = this.Bounce;
            if (handler != null)
                handler(this, new IRCBounceEventArgs(GenericMessageExtractor(message)));
        }

        private void OnLocalUsersCounts(IRCMessage message)
        {
            DispatchServerMessageEvent(this.LocalUsersCounts, message);
        }

        private void OnLocalUserOperators(IRCMessage message)
        {
            DispatchServerMessageEvent(this.LocalUserOperators, message);
        }

        private void OnLocalUnknownUsers(IRCMessage message)
        {
            DispatchServerMessageEvent(this.LocalUnknownUsers, message);
        }

        private void OnLocalUserChannels(IRCMessage message)
        {
            DispatchServerMessageEvent(this.LocalUserChannels, message);
        }

        private void OnLocalUsers(IRCMessage message)
        {
            DispatchServerMessageEvent(this.LocalUsers, message);
        }

        private void OnAdminMessage(IRCMessage message)
        {
            DispatchServerMessageEvent(this.AdminMessage, message);
        }

        private void OnAdminLocation(IRCMessage message)
        {
            DispatchServerMessageEvent(this.AdminLocation, message);
        }

        private void OnAdminEmail(IRCMessage message)
        {
            DispatchServerMessageEvent(this.AdminEmail, message);
        }

        private void OnTryAgain(IRCMessage message)
        {
            DispatchServerMessageEvent(this.TryAgain, message);
        }

        private void OnNoSuchNick(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.NoSuchNick, message);
        }

        private void OnNoSuchServer(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.NoSuchServer, message);
        }

        private void OnNoSuchChannel(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.NoSuchChannel, message);
        }

        private void OnCannotSendToChannel(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.CannotSendToChannel, message);
        }

        private void OnTooManyChannels(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.TooManyChannels, message);
        }

        private void OnWasNoSuchNick(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.WasNoSuchNick, message);
        }

        private void OnTooManyTargets(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.TooManyTargets, message);
        }

        private void OnNoSuchService(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.NoSuchService, message);
        }

        private void OnNoOriginSpecified(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NoOriginSpecified, message);
        }

        private void OnNoRecipientGiven(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NoRecipientGiven, message);
        }

        private void OnNoTextToSend(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NoTextToSend, message);
        }

        private void OnNoToplevelDomainSpecified(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.NoToplevelDomainSpecified, message);
        }

        private void OnWildcardInToplevelDomain(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.WildcardInToplevelDomain, message);
        }

        private void OnBadMask(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.BadMask, message);
        }

        private void OnUnknownCommand(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.UnknownCommand, message);
        }

        private void OnNoMotd(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NoMotd, message);
        }

        private void OnNoAdminInfo(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NoAdminInfo, message);
        }

        private void OnFileError(IRCMessage message)
        {
            DispatchServerMessageEvent(this.FileError, message);
        }

        private void OnNoNicknameGiven(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NoNicknameGiven, message);
        }

        private void OnErroneousNickname(IRCMessage message)
        {
            ClearNickname();
            DispatchTargetedMessageEvent(this.ErroneousNickname, message);
        }

        private void OnNicknameInUse(IRCMessage message)
        {
            ClearNickname();
            DispatchTargetedMessageEvent(this.NicknameInUse, message);
        }

        private void OnNicknameCollision(IRCMessage message)
        {
            ClearNickname();
            DispatchTargetedMessageEvent(this.NicknameCollision, message);
        }

        private void OnUnavailableResource(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.UnavailableResource, message);
        }

        private void OnUserNotInChannel(IRCMessage message)
        {
            DispatchNickChannelMessageEent(this.UserNotInChannel, message);
        }

        private void OnNotOnChannel(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.NotOnChannel, message);
        }

        private void OnUserAlreadyOnChannel(IRCMessage message)
        {
            DispatchNickChannelMessageEent(this.UserAlreadyOnChannel, message);
        }

        private void OnNotLoggedIn(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.NotLoggedIn, message);
        }

        private void OnSummonDisabled(IRCMessage message)
        {
            DispatchServerMessageEvent(this.SummonDisabled, message);
        }

        private void OnUsersDisabled(IRCMessage message)
        {
            DispatchServerMessageEvent(this.UsersDisabled, message);
        }

        private void OnNeedMoreParameters(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.NeedMoreParameters, message);
        }

        private void OnAlreadyRegistered(IRCMessage message)
        {
            DispatchServerMessageEvent(this.AlreadyRegistered, message);
        }

        private void OnUnprivilegedHost(IRCMessage message)
        {
            DispatchServerMessageEvent(this.UnprivilegedHost, message);
        }

        private void OnPasswordMismatch(IRCMessage message)
        {
            DispatchServerMessageEvent(this.PasswordMismatch, message);
        }

        private void OnBanned(IRCMessage message)
        {
            DispatchServerMessageEvent(this.Banned, message);
        }

        private void OnYouWillBeBanned(IRCMessage message)
        {
            DispatchServerMessageEvent(this.YouWillBeBanned, message);
        }

        private void OnChannelKeyAlreadySet(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.ChannelKeyAlreadySet, message);
        }

        private void OnChannelIsFull(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.ChannelIsFull, message);
        }

        private void OnUnknownChannelMode(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.UnknownChannelMode, message);
        }

        private void OnInviteOnlyChannel(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.InviteOnlyChannel, message);
        }

        private void OnBannedFromChannel(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.BannedFromChannel, message);
        }

        private void OnBadChannelKey(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.BadChannelKey, message);
        }

        private void OnBadChannelMask(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.BadChannelMask, message);
        }

        private void OnChannelModesUnsupported(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.ChannelModesUnsupported, message);
        }

        private void OnBanListFull(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.BanListFull, message);
        }

        private void OnNoPrivileges(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NoPrivileges, message);
        }

        private void OnChannelOperatorPrivilegeNeeded(IRCMessage message)
        {
            DispatchTargetedMessageEvent(this.ChannelOperatorPrivilegeNeeded, message);
        }

        private void OnCannotKillServer(IRCMessage message)
        {
            DispatchServerMessageEvent(this.CannotKillServer, message);
        }

        private void OnRestrictedConnection(IRCMessage message)
        {
            DispatchServerMessageEvent(this.RestrictedConnection, message);
        }

        private void OnNotOriginalChannelOperator(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NotOriginalChannelOperator, message);
        }

        private void OnNotOperatorHost(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NotOperatorHost, message);
        }

        private void OnUnknownModeFlag(IRCMessage message)
        {
            DispatchServerMessageEvent(this.UnknownModeFlag, message);
        }

        private void OnNotYourUser(IRCMessage message)
        {
            DispatchServerMessageEvent(this.NotYourUser, message);
        }

        private void OnUserHost(IRCMessage message)
        {
            if (this.UserHost != null)
            {
                string[] param = message.Parameters;
                string recipient = param[0];

                int i = 1;
                while (i < param.Length)
                {
                    string p = param[i];

                    // we can have empty parameters by the very nature of the fact that we split on ' '
                    if (!p.Trim().Equals("") && !p.Trim().Equals(":"))
                    {
                        if (p[0] == ':')
                            p = p.Substring(1); // drop the leading :

                        string[] pParts = p.Split('=');
                        string lhs = pParts[0];
                        string rhs = pParts[1];

                        // parse the left hand side
                        bool oper = false;
                        string nickname = lhs;

                        if (lhs.EndsWith("*"))
                        {
                            oper = true;
                            nickname = lhs.Substring(0, lhs.Length - 1);
                        }

                        bool away = rhs[0] == '-';
                        string hostname = rhs.Substring(1);

                        this.UserHost(this, new IRCUserHostEventArgs(nickname, oper, away, hostname));
                    }

                    i++;
                }
            }
        }

        private void OnIsOn(IRCMessage message)
        {
            if (this.IsOn != null)
            {
                List<string> nicks = new List<string>();

                foreach(string nick in message.Parameters)
                {
                    nicks.Add(nick);
                }

                this.IsOn(this, new IRCIsOnEventArgs(nicks));
            }
        }

        private void OnAway(IRCMessage message)
        {
            if (this.Away != null)
            {
                this.Away(this, new IRCAwayEventArgs(message.Parameters[0],
                    TargetedMessageExtractor(message)));
            }
        }

        private void OnUnAway(IRCMessage message)
        {
            if (this.UnAway != null)
            {
                this.UnAway(this, new IRCServerMessageEventArgs(GenericMessageExtractor(message)));
            }
        }

        private void OnNowAway(IRCMessage message)
        {
            if (this.NowAway != null)
            {
                this.NowAway(this, new IRCServerMessageEventArgs(GenericMessageExtractor(message)));
            }
        }

        public void OnWhoIsUser(IRCMessage message)
        {
            if (this.WhoIsUser != null)
            {
                this.WhoIsUser(this, new IRCWhoIsUserEventArgs(
                    message.Parameters[0],
                    message.Parameters[1],
                    message.Parameters[2],
                    MessageExtractor(message, 4)));
            }
        }

        public void OnWhoIsServer(IRCMessage message)
        {
            if (this.WhoIsServer != null)
            {
                this.WhoIsServer(this, new IRCWhoIsServerEventArgs(
                    message.Parameters[0],
                    message.Parameters[1],
                    MessageExtractor(message, 2)));
            }
        }

        public void OnWhoIsOperator(IRCMessage message)
        {
            if (this.WhoIsOperator != null)
            {
                this.WhoIsOperator(this, new IRCTargetedMessageEventArgs(message.Parameters[0],
                    MessageExtractor(message, 1)));
            }
        }

        public void OnWhoIsIdle(IRCMessage message)
        {
            if (this.WhoIsIdle != null)
            {
                this.WhoIsIdle(this, new IRCWhoIsIdleEventArgs(message.Parameters[0],
                    long.Parse(message.Parameters[1]),
                    MessageExtractor(message, 2)));
            }
        }

        public void OnEndOfWhoIs(IRCMessage message)
        {
            if (this.EndOfWhoIs != null)
            {
                this.EndOfWhoIs(this, new IRCTargetedMessageEventArgs(message.Parameters[0], TargetedMessageExtractor(message)));
            }
        }

        public void OnWhoIsChannels(IRCMessage message)
        {
            if (this.WhoIsChannels != null)
            {
                List<IRCUserChannel> userChannels = new List<IRCUserChannel>();
                string target = message.Parameters[0];

                int i = 1;
                while (i < message.Parameters.Length)
                {
                    string messagePart = message.Parameters[i];
                    if (messagePart.StartsWith(":*"))
                        messagePart = messagePart.Substring(2);

                    bool oper = messagePart[0] == '*';
                    bool canSpeak = messagePart[0] == '+';
                    string channel;

                    if (oper || canSpeak)
                        channel = messagePart.Substring(1);
                    else
                        channel = messagePart;

                    userChannels.Add(new IRCUserChannel
                    {
                        Nick = target,
                        Channel = channel,
                        Operator = oper,
                        CanSpeak = canSpeak
                    });

                    i++;
                }
            }
        }

        public void OnWhoWasUser(IRCMessage message)
        {
            if (this.WhoIsUser != null)
            {
                this.WhoWasUser(this, new IRCWhoWasUserEventArgs(
                    message.Parameters[0],
                    message.Parameters[1],
                    message.Parameters[2],
                    MessageExtractor(message, 4)));
            }
        }

        public void OnEndOfWhoWas(IRCMessage message)
        {
            if (this.EndOfWhoWas != null)
            {
                this.EndOfWhoWas(this, new IRCTargetedMessageEventArgs(message.Parameters[0], TargetedMessageExtractor(message)));
            }
        }

        private void ClearNickname()
        {
            this.CurrentNick = null;
        }

        // helper methods

        private string MessageExtractor(IRCMessage message, int messageStart)
        {
            return StripPrefix(String.Join(" ", message.Parameters, messageStart, message.Parameters.Length - messageStart - 1));
        }

        private string TargetedMessageExtractor(IRCMessage message)
        {
            // strip off the :, return everything else
            return MessageExtractor(message, 1);
        }

        private string GenericMessageExtractor(IRCMessage message)
        {
            // strip off the :, return everything else
            return MessageExtractor(message, 0);
        }

        private string StripPrefix(string messageText)
        {
            if (messageText[0] == prefixChar)
                return messageText.Substring(1);
            else
                return messageText;
        }

        private void DispatchServerMessageEvent(EventHandler<IRCServerMessageEventArgs> handler, IRCMessage message)
        {
            if (handler != null)
            {
                handler(this, new IRCServerMessageEventArgs(GenericMessageExtractor(message)));
            }
        }

        private void DispatchTargetedMessageEvent(EventHandler<IRCTargetedMessageEventArgs> handler, IRCMessage message)
        {
            if (handler != null)
            {
                string target = message.Parameters[0];

                handler(this, new IRCTargetedMessageEventArgs(target, TargetedMessageExtractor(message)));
            }
        }

        private void DispatchNickChannelMessageEent(EventHandler<IRCNickChannelMessageEventArgs> handler, IRCMessage message)
        {
            if(handler != null)
            {
                string nick = message.Parameters[0];
                string channel = message.Parameters[1];
                string messageText = StripPrefix(String.Join(" ", message.Parameters, 2, message.Parameters.Length));

                handler(this, new IRCNickChannelMessageEventArgs(nick, channel, messageText));
            }
        }

        // User commands

        public void Pong(string requestor)
        {
            System.Diagnostics.Debug.WriteLine("Pong " + requestor);

            IRCMessage pong = new IRCMessage(null, "PONG", requestor);
            SendMessage(pong);
        }

        public void Nick(string newNick)
        {
            IRCMessage nick = new IRCMessage(null, "NICK", newNick);
            SendMessage(nick);

            this.CurrentNick = newNick;
        }

        public void User(string username, string realname)
        {
            IRCMessage user = new IRCMessage(null, "USER", username, "8", "*", ":" + realname);
            SendMessage(user);
        }

        public void Password(string password)
        {
            IRCMessage user = new IRCMessage(null, "PASS", password);
            SendMessage(user);
        }

        public void Register(string password, string nick, string username, string realname)
        {
            if (password != null)
                Password(password);

            Nick(nick);

            User(username, realname);
        }
    }

    public class IRCMessage
    {
        private string _command;
        public string Command
        {
            get { return this._command; }
        }

        string[] _parameters;
        public string[] Parameters
        {
            get { return this._parameters; }
        }

        string _prefix;
        public string Prefix
        {
            get { return this._prefix; }
        }

        public IRCMessage(string prefix, string command, params string[] parameters)
        {
            this._prefix = prefix;
            this._command = command;
            this._parameters = parameters;
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();

            sb.Append("{ Prefix = ")
                .Append(this._prefix)
                .Append(", Command = ")
                .Append(this._command)
                .Append(", Parameters = { ")
                .Append(String.Join(", ", this._parameters))
                .Append(" } }");

            return sb.ToString();
        }
    }
}
