﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCNotRegisteredEventArgs : EventArgs
    {
        private string _message;
        public string Message
        {
            get { return this._message; }
        }

        public IRCNotRegisteredEventArgs(string message)
            : base()
        {
            this._message = message;
        }
    }
}
