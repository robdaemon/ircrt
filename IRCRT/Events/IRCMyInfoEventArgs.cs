﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCMyInfoEventArgs : ServerMessageEventArgs
    {
        public IRCMyInfoEventArgs(string recipient, string message) : base(recipient, message) { }
    }
}
