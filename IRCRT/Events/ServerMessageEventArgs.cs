﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public abstract class ServerMessageEventArgs : EventArgs
    {
        private string _recipient;
        public string Recipient
        {
            get { return this._recipient; }
        }

        private string _message;
        public string Message
        {
            get { return this._message; }
        }

        public ServerMessageEventArgs(string recipient, string message)
            : base()
        {
            this._recipient = recipient;
            this._message = message;
        }
    }
}
