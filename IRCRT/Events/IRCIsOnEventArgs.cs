﻿using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCIsOnEventArgs : EventArgs
    {
        private List<string> _nicks = new List<string>();
        public List<string> Nicks
        {
            get { return this._nicks; }
        }

        public IRCIsOnEventArgs(List<string> nicks) 
            : base()
        {
            this._nicks.AddRange(nicks);
        }
    }
}
