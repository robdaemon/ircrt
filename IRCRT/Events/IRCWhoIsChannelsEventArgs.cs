﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using IRCRT.Models;

namespace IRCRT.Events
{
    public class IRCWhoIsChannelsEventArgs : EventArgs
    {
        private List<IRCUserChannel> _userChannels = new List<IRCUserChannel>();
        public List<IRCUserChannel> UserChannels
        {
            get { return this._userChannels; }
        }

        public IRCWhoIsChannelsEventArgs(List<IRCUserChannel> userChannels)
            : base()
        {
            this._userChannels.AddRange(userChannels);
        }
    }

}
