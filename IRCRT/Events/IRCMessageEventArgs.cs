﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCMessageEventArgs : EventArgs
    {
        private string _recipient;
        public string Recipient
        {
            get { return this._recipient; }
        }

        private string _senderNickname;
        public string SenderNickname
        {
            get { return this._senderNickname; }
        }

        private string _senderUser;
        public string SenderUser
        {
            get { return this._senderUser; }
        }

        private string _message;
        public string Message
        {
            get { return this._message; }
        }

        public IRCMessageEventArgs(string recipient, string senderNickname, string senderUser, string message)
            : base()
        {
            this._recipient = recipient;
            this._senderNickname = senderNickname;
            this._senderUser = senderUser;
            this._message = message;
        }
    }
}
