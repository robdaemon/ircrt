﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCYourHostEventArgs : ServerMessageEventArgs
    {
        public IRCYourHostEventArgs(string recipient, string message) : base(recipient, message) { }
    }
}
