﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCWhoIsServerEventArgs : EventArgs
    {
        private string _nick;
        public string Nick
        {
            get { return this._nick; }
        }

        private string _server;
        public string Server
        {
            get { return this._server; }
        }

        private string _serverInfo;
        public string ServerInfo
        {
            get { return this._serverInfo; }
        }

        public IRCWhoIsServerEventArgs(string nick, string server, string serverInfo)
            : base()
        {
            this._nick = nick;
            this._server = server;
            this._serverInfo = serverInfo;
        }
    }
}
