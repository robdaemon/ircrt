﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCPingEventArgs : EventArgs
    {
        private string _requestor;
        public string Requestor
        {
            get { return this._requestor; }
        }

        public IRCPingEventArgs(string requestor)
            : base()
        {
            this._requestor = requestor;
        }
    }
}
