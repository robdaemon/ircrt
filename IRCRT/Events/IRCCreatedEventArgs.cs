﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCCreatedEventArgs : ServerMessageEventArgs
    {
        public IRCCreatedEventArgs(string recipient, string message) : base(recipient, message) { }
    }
}
