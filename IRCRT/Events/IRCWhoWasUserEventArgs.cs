﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCWhoWasUserEventArgs : EventArgs
    {
        private string _nick;
        public string Nick
        {
            get { return this._nick; }
        }

        private string _user;
        public string User
        {
            get { return this._user; }
        }

        private string _host;
        public string Host
        {
            get { return this._host; }
        }

        private string _realName;
        public string RealName
        {
            get { return this._realName; }
        }

        public IRCWhoWasUserEventArgs(string nick, string user, string host, string realName)
            : base()
        {
            this._nick = nick;
            this._user = user;
            this._host = host;
            this._realName = realName;
        }
    }
}
