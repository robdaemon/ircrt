﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCNoticeEventArgs : EventArgs
    {
        private string _target;
        private string Target
        {
            get { return this._target; }
        }

        private string _message;
        private string Message
        {
            get { return this._message; }
        }

        public IRCNoticeEventArgs(string target, string message) : base()
        {
            this._target = target;
            this._message = message;
        }
    }
}
