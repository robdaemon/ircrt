﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCAwayEventArgs : EventArgs
    {
        private string _nick;
        public string Nick
        {
            get { return this._nick; }
        }

        private string _message;
        public string Message
        {
            get { return this._message; }
        }

        public IRCAwayEventArgs(string nick, string message)
            : base()
        {
            this._nick = nick;
            this._message = message;
        }
    }
}
