﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCUserHostEventArgs : EventArgs
    {
        private string _nickname;
        public string Nickname
        {
            get { return this._nickname; }
        }

        private bool _operator;
        public bool Operator
        {
            get { return this._operator; }
        }

        private bool _away;
        public bool Away
        {
            get { return this._away; }
        }

        private string _hostname;
        public string Hostname
        {
            get { return this._hostname; }
        }

        public IRCUserHostEventArgs(string nickname, bool oper, bool away, string hostname) 
            : base() 
        {
            this._nickname = nickname;
            this._operator = oper;
            this._away = away;
            this._hostname = hostname;
        }
    }
}
