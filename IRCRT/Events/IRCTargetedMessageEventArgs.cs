﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCTargetedMessageEventArgs : EventArgs
    {
        private string _target;
        public string Target
        {
            get { return this._target; }
        }

        private string _message;
        public string Message
        {
            get { return this._message; }
        }

        public IRCTargetedMessageEventArgs(string target, string message)
            : base()
        {
            this._target = target;
            this._message = message;
        }
    }
}
