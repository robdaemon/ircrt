﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCWhoIsIdleEventArgs : EventArgs
    {
        private string _nick;
        public string Nick
        {
            get { return this._nick; }
        }

        private long _seconds;
        public long Seconds
        {
            get { return this._seconds; }
        }

        private string _message;
        public string Message
        {
            get { return this._message; }
        }

        public IRCWhoIsIdleEventArgs(string nick, long seconds, string message)
            : base()
        {
            this._nick = nick;
            this._seconds = seconds;
            this._message = message;
        }
    }
}
