﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCNickChannelMessageEventArgs : EventArgs
    {
        private string _nick;
        public string Nick
        {
            get { return this._nick; }
        }

        private string _channel;
        public string Channel
        {
            get { return this._channel; }
        }

        private string _message;
        public string Message
        {
            get { return this._message; }
        }

        public IRCNickChannelMessageEventArgs(string nick, string channel, string message)
            : base()
        {
            this._nick = nick;
            this._channel = channel;
            this._message = message;
        }
    }
}
