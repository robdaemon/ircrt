﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IRCRT.Events
{
    public class IRCWelcomeEventArgs : ServerMessageEventArgs
    {
        public IRCWelcomeEventArgs(string recipient, string message) : base(recipient, message) { }
    }
}
