﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestPlatform.UnitTestFramework;

using IRCRT;
using IRCRT.Events;
using IRCRT.Commands;

namespace IRCRT.Tests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            bool quit = false;
            bool done = false;
            bool connected = false;
            bool error = false;

            IRCClient client = new IRCClient("localhost", 6667, false);

            client.Connected += (source, e) =>
            {
                connected = true;

                client.Register(null, "robdaemon", "rob", "Rob Roland");
            };
            client.Notice += (source, e) => {
                int i = 0;
            };
            client.Disconnected += (source, e) =>
            {
                done = true;
            };
            client.Error += (source, e) =>
            {
                error = true;
            };
            client.NotRegistered += (source, e) =>
            {
                client.Register(null, "robdaemon", "rob", "Robert Roland");
            };
            client.Message += (source, e) =>
            {
                System.Diagnostics.Debug.WriteLine("Private message from " + e.SenderNickname + " to " + e.Recipient + " with text " + e.Message);

                // temporary - allows me to exit cleanly
                if (e.Message.Equals("quit"))
                    quit = true;
            };
            client.Welcome += (source, e) =>
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            };
            client.MyInfo += (source, e) =>
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            };
            client.Created += (source, e) =>
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            };
            client.YourHost += (source, e) =>
            {
                System.Diagnostics.Debug.WriteLine(e.Message);
            };
            client.UserHost += (source, e) =>
            {
                System.Diagnostics.Debug.WriteLine(
                    String.Format("User {0} has host {1}, Away: {2}, Oper: {3}",
                    e.Nickname, e.Hostname, e.Away, e.Operator));
            };

            // don't connect until after we attach our event handlers.
            client.Connect();

            int pass = 0;

            while (!done)
            {
                if (quit)
                {
                    Quit cmd = new Quit("hasta la pasta");
                    client.SendCommand(cmd);
                }

                if (pass == 1)
                {
                    UserHost cmd = new UserHost();
                    cmd.Nicks.Add("rob2");
                    cmd.Nicks.Add("robdaemon");

                    client.SendCommand(cmd);
                }

                if (pass == 2)
                {
                    WhoIs cmd = new WhoIs("rob2");

                    client.SendCommand(cmd);
                }

                pass++;

                new System.Threading.ManualResetEvent(false).WaitOne(2000);
            }

            Assert.IsTrue(connected);
            Assert.IsFalse(error);
        }
    }
}
